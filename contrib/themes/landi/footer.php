<?php global $redux_demo; ?>
			</div> <!-- /container -->
			<div class="footer-cta">
				<div class="container-fluid">
						<div class="pull-left col-md-1 vcenter"><img src="<?php echo $redux_demo['footer-cta-left-image']['url']; ?>" width="90" heigth="90" /></div>
						<div class="pull-left col-md-4 vcenter"><?php echo $redux_demo['footer-cta-left']; ?></div>
						<div class="pull-right col-md-2 vcenter text-right" style="margin-top: 20px;"><?php echo $redux_demo['footer-cta-right']; ?></div>
				</div>
			</div>
			<footer>
				<div class="footer-widgets-panel">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-2 text-center bottom"><a class="img-responsive" href="<?php echo site_url(); ?>"> <img class="img-responsive" alt="Landi Renzo - Gas Specialist" src="<?php echo get_template_directory_uri() . '/assets/images/logo-landi-footer.svg' ?>"/></a><?php dynamic_sidebar( 'footer-column-1' ); ?></div>
							<div class="col-md-5 col-md-offset-1 bottom"><?php dynamic_sidebar( 'footer-column-2' ); ?></div>
							<div class="col-md-2 col-md-offset-1 bottom"><?php dynamic_sidebar( 'footer-column-3' ); ?>
							<?php wp_nav_menu( array(
							'menu'              => 'footer-menu',
							'theme_location'    => 'footer-menu',
							'depth'             => 2,
							'container'         => false
								)
							); ?>
							</div>
							<div class="col-md-1 bottom"><?php dynamic_sidebar( 'footer-column-4' ); ?>
							<h5 class="pull-right"><a title="Digital Marketing" target="_blank" href="http://www.websolute.com">
<img src="<?php echo get_template_directory_uri() . '/assets/images/DigitalMarketing.svg'?>" alt="Digital Marketing" width="15" height="15" style="vertical-align: middle;"> websolute
</a></h5>
</div>
						</div>
					</div>
				</div>
			</footer>
			<?php wp_footer(); ?>
			<script type="text/javascript"><?php echo $redux_demo['tracking-code']; ?></script>
		</div> <!-- /mmenu wrapper -->
	</body>
</html>