<?php global $redux_demo; ?>
<?php global $current_user; ?>
<!DOCTYPE html>
<html lang="it">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="<?php echo get_template_directory_uri() . '/assets/images/favicon.png' ?>">
        <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri() . '/assets/images/apple-touch-icon.png' ?>">
        <title><?php wp_title( '|' , 1, 'right' ); ?> <?php bloginfo( 'name' ); ?></title>
		<link href='https://fonts.googleapis.com/css?family=Lato:400,900' rel='stylesheet' type='text/css'>
        <?php wp_head(); ?>
    </head>

    <body <?php body_class( $class ); ?>>
        <!-- Primary Menu-->
        <?php if ( !empty($redux_demo['stage-navbar-style'] ) ) : ?>
            <nav class="navbar" role="navigation">
        <?php else : ?>
            <nav class="navbar mm-fixed-top" role="navigation">
        <?php endif; ?>
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand img-responsive" href="<?php echo site_url(); ?>"> <img class="img-responsive" alt="Landi Renzo - Gas Specialist" src="<?php echo get_template_directory_uri() . '/assets/images/logo-default.svg' ?>"/></a>

            <div class="top-cta"><?php echo $redux_demo['top-editor-text']; ?></div><div class="btn-main-menu"><a id="nav-toggle"  type="button"  data-toggle="modal" data-target="#menuModal"><small>CLOSE MENU</small><span></span></a></div>
        </nav>

        <div class="modal fade fullscreen" id="menuModal"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">


                <div class="modal-body">
	                <div class="row nomar ">
		            <div class="col-md-6 nopad left-menu">
                    <?php wp_nav_menu(
                        array(
                            'menu'              => 'primary-menu',
                            'theme_location'    => 'primary-menu',
                            'depth'             => 2,
                            'menu_class'        => 'menu-dx',
                            )
                        );
                    ?>
                    <div class="menu-sx-content"><?php echo $redux_demo['sx-editor-text']; ?></div>
		            </div>
		                     <div class="col-md-6 nopad right-menu" style="background-image: url(<?php echo $redux_demo['menu-bg']['background-image']; ?>);  background-position: <?php echo $redux_demo['menu-bg']['background-position']; ?>; background-repeat:<?php echo $redux_demo['menu-bg']['background-repeat']; ?>; background-size: <?php echo $redux_demo['menu-bg']['background-size']; ?>;">

			                     <div class="menu-dx-content"><?php echo $redux_demo['editor-text']; ?></div>


			                 </div>
	                </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.fullscreen -->
        <div class="container-fluid" id="page-content">